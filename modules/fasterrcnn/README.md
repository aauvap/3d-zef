### Introduction to train your own Faster RCNN model

1. Download the 3D-ZeF folder (14.0 GB) from motchallenge.net/data/3D-ZeF20/

2. Unzip the '3DZeF20.zip' folder into the '3d-zef' main folder, such that you get the following structure:
    * `/your/path/3d-zef/3DZeF20/`

3. Create a conda environment from `environment.yml`
    * `$ conda env create --file environment.yml`
    * This will create a simple conda environment named "3dzef" with pytorch, torchvision and pycocotools.

5. Activate the conda environment

6. Run the script `3d-zef/modules/dataset_processing/headers2GTFiles.sh`

7. Ensure that your groundtruth files have headers:
    * Look for `/3d-zef/3DZeF20/train/ZebraFish-01/gt/gt.txt`
    * The expected header should be:
    ```
        frame,id,3d_x,3d_y,3d_z,camT_x,camT_y,camT_left,camT_top,camT_width,camT_height,camT_occlusion,camF_x,camF_y,camF_left,camF_top,camF_width,camF_height,camF_occlusion
    ```

8. Run the script `3d-zef/modules/dataset_processing/gatherTrainImgs.py` in order to create symlinks of all the training images into a top and front folder as well as simplified and gathered ground truth files
    * The data will be placed under `3d-ZeF/3DZeF20/train/all/`

9. Run `train.py` to train a fasterrcnn model for 1 epoch
